package com.ycclubed.aaron.btlocktest;

import android.bluetooth.BluetoothDevice;
import android.content.Intent;
import android.app.AlertDialog;
import android.bluetooth.BluetoothAdapter;
import android.content.DialogInterface;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private BluetoothAdapter BTAdapter;
    private ArrayList<DeviceItem> BTDevices;
    public static int REQUEST_BLUETOOTH = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        RecyclerView rv = (RecyclerView)findViewById(R.id.rv);
        LinearLayoutManager llm = new LinearLayoutManager(this);
        rv.setLayoutManager(llm);

        BTAdapter = BluetoothAdapter.getDefaultAdapter();
        // If phone does not support Bluetooth, let the user know and exit
        if (BTAdapter == null) {
                // Instantiate an AlertDialog.Builder with its constructor
                AlertDialog.Builder noBT = new AlertDialog.Builder(this);
                // Chain together various setter methods to set the dialog characteristics
                noBT.setTitle("Not Compatible")
                        .setMessage("Your phone does not support Bluetooth")
                        .setPositiveButton("Exit", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                System.exit(0);
                            }
                        }).setIcon(android.R.drawable.ic_dialog_alert).create().show();
                        // Called create() on the object and then call show() just to be safe,
                        // but also works with just show(). Not sure what the implications of this are.

        }

        // If Bluetooth is not enabled
        if (!BTAdapter.isEnabled()) {
            // Create an intent with a given action
            Intent enableBT = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            // start activity and receive a result back, returns REQUEST_BLUETOOTH code when activity exits
            startActivityForResult(enableBT, REQUEST_BLUETOOTH);
        }

        // BTAdapter.getBondedDevices() gets a Set, convert into ArrayList for easy manipulation
        BTDevices = new ArrayList<DeviceItem>();
        if (BTAdapter.getBondedDevices().size() > 0) {
            for (BluetoothDevice device : BTAdapter.getBondedDevices()) {
                DeviceItem newDevice= new DeviceItem(device.getName(),device.getAddress(),"false");
                BTDevices.add(newDevice);
            }
        }
        // Show RecyclerView adapter
        RVAdapter adapter = new RVAdapter(BTDevices);
        rv.setAdapter(adapter);

    }
}
