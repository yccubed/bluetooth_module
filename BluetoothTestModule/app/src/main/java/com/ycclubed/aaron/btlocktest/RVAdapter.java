package com.ycclubed.aaron.btlocktest;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;


/**
 * Created by Aaron on 2/8/2016.
 */
public class RVAdapter extends RecyclerView.Adapter<RVAdapter.BTViewHolder>{
    ArrayList<DeviceItem> devices;

    public static class BTViewHolder extends RecyclerView.ViewHolder {
        CardView cv;
        TextView DeviceName;
        TextView DeviceMAC;

        BTViewHolder(View itemView) {
            super(itemView);
            cv = (CardView)itemView.findViewById(R.id.cv);
            DeviceName = (TextView)itemView.findViewById(R.id.device_name);
            DeviceMAC = (TextView)itemView.findViewById(R.id.macAddress);
        }
    }

    RVAdapter(ArrayList<DeviceItem> devices){
        this.devices = devices;
    }

    @Override
    public int getItemCount() {
        return devices.size();
    }

    @Override
    public BTViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item, viewGroup, false);
        BTViewHolder btvh = new BTViewHolder(v);
        return btvh;
    }

    @Override
    public void onBindViewHolder(BTViewHolder bTViewHolder, int i) {
        bTViewHolder.DeviceName.setText(devices.get(i).getDeviceName());
        bTViewHolder.DeviceMAC.setText(devices.get(i).getAddress());
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }
}
