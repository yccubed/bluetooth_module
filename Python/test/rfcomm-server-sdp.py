import bluetooth
import time

server_sock=bluetooth.BluetoothSocket( bluetooth.RFCOMM )

port = 1
server_sock.bind(("",bluetooth.PORT_ANY))
server_sock.listen(1)
print "listening on port %d" % port

uuid = "45ec5393-c64c-f33c-3a8f-258874c0f198"
bluetooth.advertise_service( server_sock, "YCCubed Service", uuid )

client_sock,address = server_sock.accept()
print "Accepted connection from ",address

data = client_sock.recv(1024)
print "received [%s]" % data

client_sock.close()
server_sock.close()

while True:
    print "Checking if Bluetooth is in range"

    result = bluetooth.lookup_name('78:F8:82:55:B0:46', timeout=5)
    if (result != None):
        print "Aaron: in"
    else:
        print "Aaron: out"

    time.sleep(60)